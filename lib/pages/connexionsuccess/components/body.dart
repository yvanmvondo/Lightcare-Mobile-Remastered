import 'package:flutter/material.dart';
import 'package:lightcare/components/default_bouton.dart';
import 'package:lightcare/pages/home/accueil.dart';
import 'package:lightcare/configuration.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.screenHeight * 0.03, width: double.infinity,),
        Image.asset(
          "assets/images/theboy.png",
          height: SizeConfig.screenHeight * 0.4, //40%
        ),
        SizedBox(height: SizeConfig.screenHeight * 0.08),
        Text(
          " LIGHTCARE",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(30),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Spacer(),
        SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          child: DefaultBouton(
            text: "Tableau de bord",
            press: () {
              Navigator.pushNamed(context, Accueil.routeName);
            },
          ),
        ),
        Spacer(),
      ],
    );
  }
}