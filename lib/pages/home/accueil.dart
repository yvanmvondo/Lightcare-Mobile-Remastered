import 'package:flutter/material.dart';

import 'components/body.dart';
import '../../constantes.dart';

class Accueil extends StatelessWidget {
  static String routeName = "/home";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Tableau de bord",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: kBackgroundColor,
        textTheme: Theme.of(context).textTheme.apply(displayColor: kTextColor),
      ),
      home: Body(),
    );
  }
}
