import 'package:flutter/material.dart';
import 'components/body.dart';

class ConnexionScreen extends StatelessWidget {
  static String routeName = "/connect";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Connexion"),
      ),
      body: Body(),
    );
  }
}
