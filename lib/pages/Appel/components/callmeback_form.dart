import 'package:flutter/material.dart';
import 'package:lightcare/components/suffix_icon.dart';
import 'package:lightcare/components/form_error.dart';

import '../../../components/default_bouton.dart';
import '../../../constantes.dart';
import '../../../configuration.dart';

class CallmebackForm extends StatefulWidget {
  @override
  _CallmebackFormState createState() => _CallmebackFormState();
}

class _CallmebackFormState extends State<CallmebackForm> {
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  String phone;
  String name;
  String service;
  String town;
  String hospital;
  String gender;
  String date;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPhoneFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildGenderFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildServiceFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDateFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildHopitalFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTownFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          DefaultBouton(
            text: "Continue",
            press: () {
              // Navigator.pushNamed(context, ConfirmationScreen.routeName);
            },
          ),
        ],
      ),
    );
  }


  TextFormField buildNameFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => name = newValue,
      decoration: InputDecoration(
        labelText: "Nom et Prénom",
        hintText: "Entrer votre nom Complet",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "Entrer votre mot de passe",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }

  TextFormField buildGenderFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      decoration: InputDecoration(
        labelText: "Genre",
        hintText: "Selectionner un genre",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Star Icon.svg"),
      ),
    );
  }

  TextFormField buildServiceFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      decoration: InputDecoration(
        labelText: "Service",
        hintText: "Choisir un service",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Parcel.svg"),
      ),
    );
  }

  TextFormField buildHopitalFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => hospital = newValue,
      decoration: InputDecoration(
        labelText: "Centre hospitalier",
        hintText: "Sélectionner un Hopital",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Plus Icon.svg"),
      ),
    );
  }

  TextFormField buildTownFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => town = newValue,
      decoration: InputDecoration(
        labelText: "Ville",
        hintText: "Sélectionner une ville",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Bill Icon.svg"),
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => phone = newValue,
      decoration: InputDecoration(
        labelText: "Télephone",
        hintText: "Entrer votre numero de telephone",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
      ),
    );
  }

  TextFormField buildDateFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => date = newValue,
      decoration: InputDecoration(
        labelText: "Date",
        hintText: "Proposer une date",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Flash Icon.svg"),
      ),
    );
  }
}
