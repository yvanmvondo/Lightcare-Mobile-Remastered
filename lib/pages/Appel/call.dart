import 'package:flutter/material.dart';

import 'components/body.dart';

class CallScreen extends StatelessWidget {
  static String routeName = "/call";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Réservez un Rendez-vous"),
      ),
      body: Body(),
    );
  }
}