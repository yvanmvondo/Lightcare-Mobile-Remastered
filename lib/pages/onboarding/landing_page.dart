import 'package:flutter/material.dart';
import 'package:lightcare/pages/onboarding/components/body.dart';
import 'package:lightcare/configuration.dart';

class LandingPage extends StatelessWidget {
  static String routeName = "/accueil";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}