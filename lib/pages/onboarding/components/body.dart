import 'package:flutter/material.dart';
import 'package:lightcare/constantes.dart';
import 'package:lightcare/configuration.dart';
import 'package:lightcare/pages/connexion/connexion.dart';

// This is the best practice
import 'package:lightcare/pages/onboarding/components/content.dart';
import 'package:lightcare/components/default_bouton.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> splashData = [
    {
      "text": "Contactez l\'hopital le plus proche et obtenez un rendez-vous.",
      "image": "assets/images/onboarding/diagnostic.png"
    },
    {
      "text":
      "Effectuez vos diagnostics en repondant à des questions ciblées et suivez votre état de santé en 14 jours.",
      "image": "assets/images/onboarding/fortnight.png"
    },
    {
      "text": "Partagez vos astuces santé, bien-etre et surtout lisez et commentez celle des autres.",
      "image": "assets/images/onboarding/community.png"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: PageView.builder(
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: splashData.length,
                itemBuilder: (context, index) => SplashContent(
                  image: splashData[index]["image"],
                  text: splashData[index]['text'],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashData.length,
                            (index) => buildDot(index: index),
                      ),
                    ),
                    Spacer(flex: 3),
                    DefaultBouton(
                      text: "Continue",
                      press: () {
                         Navigator.pushNamed(context, ConnexionScreen.routeName);
                      },
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
