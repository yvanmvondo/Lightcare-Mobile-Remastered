import 'package:flutter/material.dart';
import 'components/body.dart';

class BlankScreen extends StatelessWidget {
  static String routeName = "/blank";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: SizedBox(),
        title: Text("En Maintenance..."),
      ),
      body: Body(),
    );
  }
}