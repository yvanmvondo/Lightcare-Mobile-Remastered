import 'package:flutter/widgets.dart';
import 'package:lightcare/pages/onboarding/Landing_Page.dart';
import 'package:lightcare/pages/connexion/connexion.dart';
import 'package:lightcare/pages/inscription/inscription.dart';
import 'package:lightcare/pages/motdepasseforgot/mdpforgot.dart';
import 'package:lightcare/pages/connexionsuccess/success.dart';
import 'package:lightcare/pages/home/accueil.dart';
import 'package:lightcare/pages/blank/blank.dart';
import 'package:lightcare/pages/community/community.dart';


final Map<String, WidgetBuilder> routes = {
  LandingPage.routeName: (context) => LandingPage(),
  ConnexionScreen.routeName: (context) => ConnexionScreen(),
  InscriptionScreen.routeName: (context) => InscriptionScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  Accueil.routeName: (context) => Accueil(),
  BlankScreen.routeName: (context) => BlankScreen(),
  CommunityScreen.routeName: (context) => CommunityScreen(),
};