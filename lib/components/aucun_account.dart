import 'package:flutter/material.dart';
import 'package:lightcare/pages/inscription/inscription.dart';
import '../constantes.dart';
import '../configuration.dart';

class NoAccountText extends StatelessWidget {
  const NoAccountText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Aucun compte? ",
          style: TextStyle(fontSize: getProportionateScreenWidth(16)),
        ),
        GestureDetector(
           onTap: () => Navigator.pushNamed(context, InscriptionScreen.routeName),
          child: Text(
            "Inscription",
            style: TextStyle(
                fontSize: getProportionateScreenWidth(16),
                color: kPrimaryColor),
          ),
        ),
      ],
    );
  }
}
