import 'package:flutter/material.dart';
import './routes.dart';
import 'package:lightcare/pages/onboarding/landing_page.dart';
import 'theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'LightCare',
      theme: theme(),
      // home: LandingPage(),
      initialRoute: LandingPage.routeName,
      routes: routes,
    );
  }
}

